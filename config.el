;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Asif Mahmud Shimon"
      user-mail-address "ams.eee09@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
(setq doom-font (font-spec :family "JetBrains Mono" :size 14 :weight 'light)
     doom-variable-pitch-font (font-spec :family "JetBrains Mono" :size 14))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'ef-dark)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
(map!
 "C-s" #'swiper
 "C-r" #'swiper
 "M-0" #'treemacs-select-window
 "C-c t t" #'treemacs)

(setq +treemacs-git-mode 'deferred
      indent-tabs-mode nil
      tab-width 2
      js-indent-level 2
      css-indent-offset 2
      typescript-indent-level 2
      eldoc-idle-delay 0.75
      flymake-no-changes-timeout 0.75
      c-basic-offset 2)

(use-package! ligature
  :config
  (ligature-set-ligatures 'prog-mode '(
                                       "==" ">=" "<=" "!=" "===" "!==" "++"
                                       "->" "<-" "-->" "<--" "<!--" "==>" "<=="
                                       "<<=" "/=" "=>>" ":=" "=>"
                                       ))
  (global-ligature-mode t))

(use-package! corfu
  :custom
  (corfu-cycle t)
  (corfu-auto t)
  (corfu-auto-delay 0)
  (corfu-auto-prefix 1)
  (corfu-separator ?\s)
  (corfu-quit-no-match t)
  (corfu-popupinfo-delay 0)
  :init
  (global-corfu-mode)
  (corfu-popupinfo-mode))

(use-package! lsp-mode
  :custom
  (lsp-completion-provider :none))

(use-package! kind-icon
  :after corfu
  :custom
  (kind-icon-default-face 'corfu-default) ;; to compute blended backgrounds correctly
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

(use-package! orderless
  :init
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil))

(use-package! beacon
  :hook
  (prog-mode . beacon-mode))

(use-package! treemacs
  :init
  (setq treemacs-eldoc-display t
        treemacs-file-event-delay 3000
        treemacs-follow-after-init t
        treemacs-silent-filewatch t
        treemacs-silent-refresh t
        treemacs-sorting 'alphabetic-asc
        treemacs-space-between-root-nodes t
        treemacs-git-mode 'extended)
  (with-eval-after-load 'treemacs
    (add-to-list 'treemacs-pre-file-insert-predicates
                 #'treemacs-is-file-git-ignored?)
    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)))

(after! js
  (define-key js-mode-map (kbd "M-.") nil))

(use-package! apheleia
  :init
  (apheleia-global-mode +1))

(add-hook! 'sql-mode-hook 'lsp)
